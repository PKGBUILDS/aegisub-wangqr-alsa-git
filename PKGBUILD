# Maintainer: ObserverOfTime <chronobserver@disroot.org>
# Based on aegisub-wangqr-git

pkgname=aegisub-wangqr-alsa-git
pkgver=3.3.3.r2.g33ff9b408
pkgrel=1
pkgdesc='A general-purpose subtitle editor (wangqr fork w/ alsa)'
arch=(x86_64)
url=http://www.aegisub.org
license=(GPL BSD)
depends=(
  alsa-lib
  boost-libs
  ffms2
  fftw
  fontconfig
  gettext
  hunspell
  icu
  libass
  libgl
  libpulse
  uchardet
  wxgtk3
  zlib
)
optdepends=(
  'libpulse: PulseAudio audio player'
)
checkdepends=(gtest)
makedepends=(boost cmake git)
provides=(aegisub aegisub-wangqr-git)
conflicts=(aegisub aegisub-git aegisub-wangqr-git)
source=('git+https://github.com/wangqr/Aegisub.git'
        'xdg.patch::https://github.com/wangqr/Aegisub/pull/132.patch')
sha256sums=('SKIP' '119a6daa2de430b439c3436d9f515c18f6d477f7f1118a246dafb8370e1df827')

pkgver() {
  git -C Aegisub describe --long --tags | \
    sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g'
}

prepare() {
  patch -sd Aegisub -p1 < xdg.patch
}

build() {
  cmake -Bbuild Aegisub \
    -DCMAKE_INSTALL_MESSAGE=NEVER \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DDEFAULT_PLAYER_AUDIO=ALSA \
    -DWITH_UPDATE_CHECKER=OFF \
    -DWITH_STARTUPLOG=OFF \
    -DWITH_PORTAUDIO=OFF \
    -DWITH_AVISYNTH=OFF \
    -DWITH_OPENAL=OFF \
    -DWITH_CSRI=OFF \
    -DWITH_OSS=OFF \
    -DWITH_TEST=ON
  cmake --build build
}

check() {
  cmake --build build --target test-aegisub
}

package() {
  DESTDIR="${pkgdir}" cmake --build build --target install
  install -Dm644 Aegisub/LICENCE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
  for file in Aegisub/automation/{autoload,include,include/aegisub}/*.*; do
    install -Dm644 "${file}" "${pkgdir}/usr/share/aegisub/${file#Aegisub/}"
  done
}
